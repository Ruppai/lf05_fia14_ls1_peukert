import java.util.Scanner; // Import der Klasse Scanner 
 
public class Rechner  
{ 
   
  public static void main(String[] args) // Hier startet das Programm 
  { 
     
    // Neues Scanner-Objekt myScanner wird erstellt     
    Scanner myScanner = new Scanner(System.in);  
     
    System.out.print("Bitte geben Sie eine ganze Zahl ein: ");    
     
    // Die Variable zahl1 speichert die erste Eingabe 
    int zahl1 = myScanner.nextInt();  
     
    System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: "); 
     
    // Die Variable zahl2 speichert die zweite Eingabe 
    int zahl2 = myScanner.nextInt();  
     
    // Die Addition der Variablen zahl1 und zahl2  
    // wird der Variable ergebnis zugewiesen. 
    int ergebnis = zahl1 + zahl2;  
     
    System.out.print("\n\n\nErgebnis der Addition lautet: "); 
    ergebnis = zahl1 + zahl2;
    System.out.print(zahl1 + " + " + zahl2 + " = " + ergebnis);   
    System.out.print("\n\n\nErgebnis der Subtraktion lautet: "); 
    ergebnis = zahl1 - zahl2;
    System.out.print(zahl1 + " - " + zahl2 + " = " + ergebnis);
    System.out.print("\n\n\nErgebnis der Multiplikation lautet lautet: "); 
    ergebnis = zahl1 * zahl2;
    System.out.print(zahl1 + " * " + zahl2 + " = " + ergebnis);
    System.out.print("\n\n\nErgebnis der Division lautet: "); 
    if(zahl2!=0) {
    ergebnis = zahl1 / zahl2;
    System.out.print(zahl1 + " + " + zahl2 + " = " + ergebnis);}
    else {
    	System.out.println("Division durch 0 ist nicht m�glich!");
    }
    myScanner.close(); 
     
  }    
}