﻿import java.util.Scanner;

class Fahrkartenautomat
{
	static double fahrkartenbestellungErfassen() {
		double gesammt;
		int anzahl;
		Scanner tastatur2 = new Scanner(System.in);
		 System.out.print("Zu zahlender Betrag (EURO): ");
	       gesammt = tastatur2.nextDouble();
	       System.out.println("Wie viele Fahrkarten möchten sie?");
	       anzahl=tastatur2.nextShort();
	       gesammt=gesammt*anzahl;
	       return gesammt;
	}
	static double fahrkartenBezahlen() {
		double ruckgeld;
		 Scanner tastatur3 = new Scanner(System.in);
		double eingezahlterGesamtbetrag = 0.0;
		double zuZahlenderBetrag=fahrkartenBezahlen();
		double eingeworfeneMünze;
	       while(eingezahlterGesamtbetrag < zuZahlenderBetrag )
	       {
	    	   System.out.print("Noch zu zahlen: ") ; 
	    	   System.out.printf(" %.2f\n", zuZahlenderBetrag - eingezahlterGesamtbetrag);
	    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
	    	   eingeworfeneMünze = tastatur3.nextDouble();
	           eingezahlterGesamtbetrag += eingeworfeneMünze;
	       }
	       ruckgeld=eingezahlterGesamtbetrag-zuZahlenderBetrag;
	       return ruckgeld;
	}
	static void fahrkartenAusgabe() {
		 System.out.println("\nFahrschein wird ausgegeben");
	       for (int i = 0; i < 8; i++)
	       {
	          System.out.print("=");
	          try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	       }
	       System.out.println("\n\n");

}
	static void rueckgeldAusgeben() {
		double rückgabebetrag = fahrkartenBezahlen();
		fahrkartenAusgabe();
	       if(rückgabebetrag > 0.0)
	       {
	    	   System.out.print("Der Rückgabebetrag in Höhe von ");
	    	   System.out.printf(" %.2f", rückgabebetrag);
	    	   System.out.print(" EURO");
	    	   System.out.println("wird in folgenden Münzen ausgezahlt:");

	           while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
	           {
	        	  System.out.println("2 EURO");
		          rückgabebetrag -= 2.0;
	           }
	           while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
	           {
	        	  System.out.println("1 EURO");
		          rückgabebetrag -= 1.0;
	           }
	           while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
	           {
	        	  System.out.println("50 CENT");
		          rückgabebetrag -= 0.5;
	           }
	           while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
	           {
	        	  System.out.println("20 CENT");
	 	          rückgabebetrag -= 0.2;
	           }
	           while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
	           {
	        	  System.out.println("10 CENT");
		          rückgabebetrag -= 0.1;
	           }
	           while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
	           {
	        	  System.out.println("5 CENT");
	 	          rückgabebetrag -= 0.05;
	           }
	       }

	       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
	                          "vor Fahrtantritt entwerten zu lassen!\n"+
	                          "Wir wünschen Ihnen eine gute Fahrt.");
	       
	      // tastatur.close();
	}
	
	
    public static void main(String[] args)
    {
    	
    	rueckgeldAusgeben();
      /* Scanner tastatur = new Scanner(System.in);
      
       double zuZahlenderBetrag; 
       double eingezahlterGesamtbetrag;
       double eingeworfeneMünze;
       double rückgabebetrag;
       short anzahl;
     */
      /* System.out.print("Zu zahlender Betrag (EURO): ");
       zuZahlenderBetrag = tastatur.nextDouble();
       System.out.println("Wie viele Fahrkarten möchten sie?");
       anzahl=tastatur.nextShort();
       zuZahlenderBetrag=zuZahlenderBetrag*anzahl;
       // Geldeinwurf
       // -----------
       */
      /* eingezahlterGesamtbetrag = 0.0;
       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
       {
    	   System.out.print("Noch zu zahlen: ") ; 
    	   System.out.printf(" %.2f\n", zuZahlenderBetrag - eingezahlterGesamtbetrag);
    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
    	   eingeworfeneMünze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneMünze;
       }
*/
       // Fahrscheinausgabe
       // -----------------
       /*System.out.println("\nFahrschein wird ausgegeben");
       for (int i = 0; i < 8; i++)
       {
          System.out.print("=");
          try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       }
       System.out.println("\n\n");
*/
       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
       /*rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
       if(rückgabebetrag > 0.0)
       {
    	   System.out.print("Der Rückgabebetrag in Höhe von ");
    	   System.out.printf(" %.2f", rückgabebetrag);
    	   System.out.print(" EURO");
    	   System.out.println("wird in folgenden Münzen ausgezahlt:");

           while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
           {
        	  System.out.println("2 EURO");
	          rückgabebetrag -= 2.0;
           }
           while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
           {
        	  System.out.println("1 EURO");
	          rückgabebetrag -= 1.0;
           }
           while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
           {
        	  System.out.println("50 CENT");
	          rückgabebetrag -= 0.5;
           }
           while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
           {
        	  System.out.println("20 CENT");
 	          rückgabebetrag -= 0.2;
           }
           while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
           {
        	  System.out.println("10 CENT");
	          rückgabebetrag -= 0.1;
           }
           while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
           {
        	  System.out.println("5 CENT");
 	          rückgabebetrag -= 0.05;
           }
       }

       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt.");
       
     */  //tastatur.close();
    }
}